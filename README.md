# IoT based Motion Detector

## Project Overview
IoT based Motion Detector is a simple project designed to detect the motion of any object. This project uses a *HC-SR501 PIR Motion Detector Sensor* to detect the motion of an object and provides the data over the internet.

As an example, say, you want to remotely detect the motion of any human being, or for that matter any object, in front of the main door of your house. This will help you to remotely understand and find out if there are any unwanted activity in front of your house.

Another typical use case could be an automatic operation of lights based on the presence of a human being in a particular area. Automatic door operations in shopping malls could also be an useful case. Enabling IoT would enhance the capability of the system and you will be able to remotely understand the frequency of usage of the system and as well, control the system remotely.

## Project Details
This project helps us to detect the presence of an object and submit the data via the internet. You can also see a report of status of the presence of the object over time.

This is a project built using the [EmMate Framework](https://mig.iquesters.com/?s=embedded) of the [Micro IoT Platform](https://mig.iquesters.com). This project works seamlessly with [SoMThing](https://mig.iquesters.com/?s=somthing)s and connects to the [migCloud](https://mig.iquesters.com/?s=cloud) portal by default.
<br>[EmMate Framework](https://mig.iquesters.com/?s=embedded) is a multi-architecture, platform independent middleware of the [Micro IoT Platform](https://mig.iquesters.com), that can be used to build any micro to small scale embedded application.
<br>[SoMThing](https://mig.iquesters.com/?s=somthing) is a hardware ecosystem consisting of SoM (the processor), Thing (the application board) and Peripherals (sensors & actuators). 
<br>[migCloud](https://mig.iquesters.com/?s=cloud) is a IoT Web Server having a configurable utilities API gateway

## Hardware Requirement
You will need a micro controller based board which has PIR sensor interface. It should also have a minimum connectivity interface like WiFi. You can power up the board with a USB cable for development purpose, but it is recommended to power up the board using a dedicated power supply for trouble free operations.

*We will use the SoMThing ecosystem, viz, SoM - System on Module, Thing Boards and Peripherals in this project.*

You will need the following:
* SoM ESP32-WROOM-32D - 1 no.
* Team Thing Development board - 1 no.
* Motion Detect Peripheral - 1 no.
* Micro-USB Cable  - 1 no.
* Adapter with a rating of 12V, 2A - 1 no.

If you do not have the required hardware, you may [explore available hardware](https://mig.iquesters.com/?s=somthing&p=resources) in the SoMThing ecosystem.

## Hardware Assembly
The hardware assembly is pretty simple. Follow the below image as a setup guide.

<img src="https://gitlab.com/micro-iot-platform/projects/motion-detector/raw/master/res/images/motion_detector.jpg" width="500">

Steps to follow:
* Take the Team Thing board
* Carefully connect the SoM in the TeamThing board
* Connect the Motion Detect Peripheral board in the mikroBUS socket.

## Software Requirement
* This project is written in C programming language.
* The project is based on the [EmMate Framework](https://mig.iquesters.com/?s=embedded&p=documentation) and thus requires the framework to build it.
<br>You must [download the EmMate framework](https://mig.iquesters.com/?s=embedded&p=downloads) and follow the [EmMate Documentation](https://mig.iquesters.com/?s=embedded&p=readme&path=master/doc/getting-started/getting_started_with_emmate.md#gettingstartedwiththeemmateframework) to start using the framework.
* You must also [download the EmMate Configurer android app](https://mig.iquesters.com/apk/EmMate_Configurer.apk), which is used to configure the hardware.
* Use the migCloud portal to access and control you hardware from the internet.

## Setting up the Project
* To setup this project, refer the [Setting up EmMate Examples and Applications](https://mig.iquesters.com/?s=embedded&p=readme&path=master/doc/getting-started/getting_started_with_emmate.md#settingupemmateexamplesandapplications) section in the [EmMate Documentation](https://mig.iquesters.com/?s=embedded&p=readme&path=master/doc/getting-started/getting_started_with_emmate.md).

## Related Documents
* SoM ESP32-WROOM-32D - [Datasheet](https://mig.iquesters.com/section/somthing/static/data-sheet/som/som-esp32-datasheet-v1.0.pdf)    [Schematic](https://mig.iquesters.com/section/somthing/static/schematic/som/som-esp32.pdf)
* TeamThing Development Board - [Datasheet](https://mig.iquesters.com/section/somthing/static/data-sheet/thing/team-thing-datasheet-v2.0.pdf)   [Schematic](https://mig.iquesters.com/section/somthing/static/schematic/thing/team-thing-v2.0.pdf)
* Motion Detect Peripheral Board - [Datasheet](https://mig.iquesters.com/section/somthing/static/data-sheet/peripheral/motion-peripheral-datasheet.pdf)  [Schematic](https://mig.iquesters.com/section/somthing/static/schematic/peripheral/motion-peripheral.pdf)

## Know More
To know more about the:
* Micro IoT Platform [click here](https://mig.iquesters.com)
* SoMThing hardware ecosystem [click here](https://mig.iquesters.com/?s=somthing)
* EmMate development framework [click here](https://mig.iquesters.com/?s=embedded)
* migCloud portal [click here](https://mig.iquesters.com/?s=cloud)
