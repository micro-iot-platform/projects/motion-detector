/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "motion_detector.h"
#include "../app-json-utils/app_json_utils.h"

#include "event_group_core.h"
#include "gpio_helper_api.h"
#include "systime.h"
#include "http_client_api.h"
#include "conn.h"
#include "apppostdata_helper.h"
#include <inttypes.h>

#define TAG	"motion_detector"

MOTION_DETECTOR_PUBLISH_DATA motion_detector_publish_data;

static TaskHandle motion_detector_publish_task_handle = NULL;
static QueueHandle motion_detector_queue = NULL;

static void INTERRUPT_ATTRIBUTES isr_func(void* arg) {
	uint32_t gpio_num = (uint32_t) arg;
	// ISR handler body
	if(motion_detector_queue!= NULL){
		QueueSendFromISR(motion_detector_queue, &gpio_num, NULL); }
}

static void motion_detector_pub_task(void *param) {

	while (1) {
		if (motion_detector_queue != NULL) {
			uint32_t io_num = -1;
			if (QueueReceive(motion_detector_queue, &io_num, THREADING_MAX_DELAY) == true) {
				if (io_num == PIR_HC_SR501_SIG_GPIO) {
					/* Debounce check */
					if (get_gpio_value(PIR_HC_SR501_SIG_GPIO) == HIGH) {
						TaskDelay(DELAY_100_MSEC / TICK_RATE_TO_MS);
						if (get_gpio_value(PIR_HC_SR501_SIG_GPIO) == HIGH) {
							EM_LOGI(TAG, "##### Motion detected ####");
							motion_detector_publish_data.motion_detect = true;
							motion_detector_publish_data.motion_detect_tm = 0;

							if (get_systime_seconds((time_t*) &motion_detector_publish_data.motion_detect_tm) == -1) {
								EM_LOGE(TAG, "Failed to fetch current Date Time");
								continue;
							} else {
								motion_detector_publish_data.motion_detect_tm =
										motion_detector_publish_data.motion_detect_tm * 1000;
							}

							char *motion_detector_pub_json_buff = NULL;
							size_t motion_detector_pub_json_buff_len = 0;
							em_err err = make_motion_detector_publish_json(&motion_detector_publish_data,
									&motion_detector_pub_json_buff, &motion_detector_pub_json_buff_len);
							if (err == EM_OK) {
//						EM_LOGI(TAG,
//								"### Motion-Detector Publish Start #############################################\n");
								EM_LOGI(TAG, "\r\n-: Motion-Detector Publish JSON :-\r\n%s",
										motion_detector_pub_json_buff);

//						if (get_network_conn_status() == NETCONNSTAT_CONNECTED) {
								em_err res = migcloud_post_app_json_http(motion_detector_pub_json_buff);
								if (res == EM_OK) {
//								EM_LOGI(TAG, "Motion-Detector Publish Process Success");
								} else if (res == EM_ERR_NO_MEM) {
									EM_LOGE(TAG, "Motion-Detector Publish Process Failed because of low memory");
								} else {
									EM_LOGE(TAG, "Motion-Detector Publish Failed");
								}
//						}
								EM_LOGI(TAG, "########\r\n");
							}
							free(motion_detector_pub_json_buff);
						}
					}
				}
			}
		} else {
			TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
		}
	}
	EM_LOGI(TAG, "Stop Motion Detector Task '%s'", __func__);
	motion_detector_publish_task_handle = NULL;
	TaskDelete(NULL);
}

void motion_detector_init() {
	/* Do all necessary initializations here */

	// configure PIR_HC_SR501_SIG_GPIO for Input
	em_err res = configure_gpio(PIR_HC_SR501_SIG_GPIO, GPIO_IO_MODE_INPUT, GPIO_IO_FLOATING);
	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to configure PIR HC-SR501 Signal GPIO");
	}

	// Add ISR function for detect Motions
	res = add_gpio_isr(PIR_HC_SR501_SIG_GPIO, GPIO_INTERRUPT_POSEDGE, isr_func, (void*)PIR_HC_SR501_SIG_GPIO);
	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to add ISR for PIR HC-SR501 Signal GPIO");
	}

	// start Motion-Detector Publish Task
	bool stat = TaskCreate(motion_detector_pub_task, "motion_dect_pub_task", TASK_STACK_SIZE_4K, NULL,
			THREAD_PRIORITY_5, &motion_detector_publish_task_handle);
	if (stat != true) {
		EM_LOGE(TAG, "Failed to start Motion Detector publish Task");
	}

	// Create Application ISR queue
	motion_detector_queue = QueueCreate(1, sizeof(uint32_t));
	if (motion_detector_queue == NULL) {
		EM_LOGE(TAG, "Failed to Create Motion Detector Queue handle");
	}
}

void motion_detector_loop() {
	// Nothing to do
}
