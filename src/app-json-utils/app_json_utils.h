/*
 * File Name: app_json_utils.h
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#ifndef APP_JSON_UTILS_H_
#define APP_JSON_UTILS_H_

#include "../motion-detector/motion_detector.h"


/*
 * Creating Application POST Json String:
 *
 {
 	 "motion_detect": 50,
 	 "motion_detect_tm" : 12457812000
 }
 *
 *
 */
em_err make_motion_detector_publish_json(MOTION_DETECTOR_PUBLISH_DATA *publish_data, char **ppbuf, size_t *plen);


#endif /* APP_JSON_UTILS_H_ */
