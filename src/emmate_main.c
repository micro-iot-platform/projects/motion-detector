
#include "motion-detector/motion_detector.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester Core Embedded Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling motion_detector_init() in your_module.c in your-module directory ...");
	motion_detector_init();
	EM_LOGI(TAG, "Returned from motion_detector_init()");

	while(1){
		//EM_LOGI(TAG, "Calling your_module_loop() in your_module.c in your-module directory ...");
		motion_detector_loop();
		//EM_LOGI(TAG, "Sleeping for %d ms before looping ...", DELAY_10_SEC);
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}